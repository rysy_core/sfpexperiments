#include "gtest/gtest.h"

extern "C" {
    #include "parse.h"
}

static int type, addr, addr_mem, value;

static void fun_pin_get(uint8_t a)
{

	type = 1;
	addr = a;
	value = -1;
	addr_mem = -1;
}

static void fun_pin_set(uint8_t a, uint8_t v)
{

	type = 1;
	addr = a;
	value = v;
	addr_mem = -1;
}

static void fun_i2c_get(uint8_t a, uint8_t a_m)
{

	type = 2;
	addr = a;
	addr_mem = a_m;
	value = -1;
}

static void fun_i2c_set(uint8_t a, uint8_t a_m, uint8_t v)
{

	type = 2;
	addr = a;
	addr_mem = a_m;
	value = v;
}

static void fun_sfp_get(uint8_t a)
{

	type = 3;
	addr = a;
}

static void fun_rec(uint8_t a, uint8_t b)
{

	type = 4;
	addr = a;
	addr_mem = b;
}

static void fun_uart_send(uint8_t a)
{

	type = 5;
	addr = a;
}

static void fun_blink(void)
{
	type = 6;
}

static void init(state *s)
{

    s->fun_pin_get = fun_pin_get;
    s->fun_pin_set = fun_pin_set;
    s->fun_i2c_get = fun_i2c_get;
    s->fun_i2c_set = fun_i2c_set;
    s->fun_sfp_get = fun_sfp_get;
    s->fun_rec = fun_rec;
    s->fun_uart_send = fun_uart_send;
    s->fun_blink = fun_blink;
}

static void send_str(state *s, const char *str)
{

    for (int i = 0; str[i] != '\0'; i++)
        parse(s, str[i]);
}


TEST(parse, parse_number)
{
    uint8_t res, n;
    n = 0;
    res = parse_number('1', &n);
    ASSERT_EQ(1, n);
    ASSERT_EQ(0, res);
    res = parse_number('2', &n);
    ASSERT_EQ(12, n);
    ASSERT_EQ(0, res);
    res = parse_number(' ', &n);
    ASSERT_EQ(12, n);
    ASSERT_EQ(1, res);
}

TEST(gcode, test_init)
{
    state s;

    parse_init(&s);

    ASSERT_EQ(s.state, 0);
}

TEST(gcode, test_commands_P_1)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "P 0\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 1);
    ASSERT_EQ(addr, 0);
    ASSERT_EQ(addr_mem, -1);
    ASSERT_EQ(value, -1);
}

TEST(gcode, test_commands_P_2)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "P 23\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 1);
    ASSERT_EQ(addr, 23);
    ASSERT_EQ(addr_mem, -1);
    ASSERT_EQ(value, -1);
}

TEST(gcode, test_commands_P_3)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "P 101 15\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 1);
    ASSERT_EQ(addr, 101);
    ASSERT_EQ(addr_mem, -1);
    ASSERT_EQ(value, 15);
}

TEST(gcode, test_commands_I_1)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "I 101 15\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 2);
    ASSERT_EQ(addr, 101);
    ASSERT_EQ(addr_mem, 15);
    ASSERT_EQ(value, -1);
}

TEST(gcode, test_commands_I_2)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "I 15 12 23\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 2);
    ASSERT_EQ(addr, 15);
    ASSERT_EQ(addr_mem, 12);
    ASSERT_EQ(value, 23);
}

TEST(gcode, test_commands_G_1)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "G\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 3);
    ASSERT_EQ(addr, 0);
}


TEST(gcode, test_commands_G_2)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "G 15\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 3);
    ASSERT_EQ(addr, 15);
}

TEST(gcode, test_commands_R_0)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "R 0 23\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 4);
    ASSERT_EQ(addr, 0);
    ASSERT_EQ(addr_mem, 23);
}

TEST(gcode, test_commands_U_0)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "Ui\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 5);
    ASSERT_EQ(addr, 'i');
}

TEST(gcode, test_commands_U_1)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "U \r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 5);
    ASSERT_EQ(addr, ' ');
}

TEST(gcode, test_commands_B_1)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "B\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 6);
}

TEST(gcode, test_commands_mix_1)
{
    state s;

    init(&s);

    parse_init(&s);

    send_str(&s, "I 15 24\r\n");

    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 2);
    ASSERT_EQ(addr, 15);
    ASSERT_EQ(addr_mem, 24);
    ASSERT_EQ(value, -1);

    send_str(&s, "P 12 4\r\n");
    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 1);
    ASSERT_EQ(addr, 12);
    ASSERT_EQ(addr_mem, -1);
    ASSERT_EQ(value, 4);

    send_str(&s, "I 21 73 45\r\n");
    ASSERT_EQ(s.state, 0);
    ASSERT_EQ(type, 2);
    ASSERT_EQ(addr, 21);
    ASSERT_EQ(addr_mem, 73);
    ASSERT_EQ(value, 45);
}
