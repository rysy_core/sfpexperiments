#ifndef __PARSE_H__
#define __PARSE_H__

#include <stdint.h>

#define PARSE_NUMS 		3

typedef struct state {
	uint8_t state;
	uint8_t code;
	uint8_t num;
	uint8_t n[PARSE_NUMS];
	uint8_t c;

	void (*fun_pin_get)(uint8_t);
	void (*fun_pin_set)(uint8_t, uint8_t);
	void (*fun_i2c_get)(uint8_t, uint8_t);
	void (*fun_i2c_set)(uint8_t, uint8_t, uint8_t);
	void (*fun_sfp_get)(uint8_t);
	void (*fun_rec)(uint8_t, uint8_t);
	void (*fun_uart_send)(uint8_t);
	void (*fun_blink)(void);
} state;

void parse_init(state *s);
void parse(state *s, char c);

uint8_t parse_number(char c, uint8_t *n);

#endif /* __PARSE_H__ */
