#ifndef __SFP_H__
#define __SFP_H__

#include <stdint.h>

void sfp_init(void);

void pin_get(uint8_t addr);
void pin_set(uint8_t addr, uint8_t val);
void i2c_get(uint8_t i2c_addr, uint8_t mem_addr);
void i2c_set(uint8_t i2c_addr, uint8_t mem_addr, uint8_t val);
void sfp_get(uint8_t addr);

#endif /* __SFP_H__ */
