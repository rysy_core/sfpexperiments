#ifndef __BOARD_H__
#define __BOARD_H__

#include "main.h"

#define	SFP_PORT 		GPIOA

#define SFP_RX_LOS 		GPIO_PIN_0
#define SFP_RS1			GPIO_PIN_1
#define SFP_RS0			GPIO_PIN_4
#define SFP_MOD_ABS		GPIO_PIN_5
#define SFP_TX_DIS		GPIO_PIN_6
#define SFP_TX_FAULT	GPIO_PIN_7

#define SFP_TX			GPIO_PIN_2
#define SFP_RX			GPIO_PIN_3

#define LED1_PORT		GPIOB
#define LED1			GPIO_PIN_1

#define LED_PORT		GPIOA
#define LED2			GPIO_PIN_13
#define LED3			GPIO_PIN_14

#endif /* __BOARD_H__ */
