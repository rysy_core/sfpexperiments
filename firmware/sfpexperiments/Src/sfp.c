#include <stdio.h>
#include "sfp.h"
#include "board.h"
#include "gpio.h"
#include "i2c.h"

enum PIN_ADDR {ADDR_TX_DIS, ADDR_RS0, ADDR_RS1, ADDR_RX_LOS, ADDR_MOD_ABS,
	ADDR_TX_FAULT};

#define ERR_PIN			255
#define SFP_I2C_ADDR_1	0xA0
#define SFP_I2C_ADDR_2	0xA2
#define SFP_I2C_TIMEOUT	100

#define SFP_MEM_SIZE	256

static inline uint16_t addr_to_pin(uint8_t addr)
{
	switch (addr) {
	case ADDR_TX_DIS:
		return SFP_TX_DIS;
	case ADDR_RS0:
		return SFP_RS0;
	case ADDR_RS1:
		return SFP_RS1;
	case ADDR_RX_LOS:
		return SFP_RX_LOS;
	case ADDR_MOD_ABS:
		return SFP_MOD_ABS;
	case ADDR_TX_FAULT:
		return SFP_TX_FAULT;
	}

	return ERR_PIN;
}

void sfp_init(void)
{

	HAL_GPIO_WritePin(SFP_PORT, SFP_TX_DIS, 1);
}

void pin_get(uint8_t addr)
{
	uint16_t pin;
	uint8_t i;
	GPIO_PinState val;

	if (addr == 10) {
		val = 0;
		for (i = 0; i <= ADDR_TX_FAULT; i++) {
			pin = addr_to_pin(i);
			val |= HAL_GPIO_ReadPin(SFP_PORT, pin) << i;
		}
	} else {
		pin = addr_to_pin(addr);
		if (pin == ERR_PIN) {
			printf("P%d: not valid\r\n", addr);
			return;
		}
		val = HAL_GPIO_ReadPin(SFP_PORT, pin);
	}

	printf("P%d: %d\r\n", addr, val);
}


void pin_set(uint8_t addr, uint8_t val)
{
	uint16_t pin;

	if (addr > ADDR_TX_FAULT) {
		printf("P%d: is not writable\r\n", addr);
		return;
	}

	pin = addr_to_pin(addr);
	if (pin == ERR_PIN) {
		printf("P%d: not valid\r\n", addr);
		return;
	}

	HAL_GPIO_WritePin(SFP_PORT, pin, val);
	printf("P%d: %d\r\n", addr, val);
}

void i2c_get(uint8_t i2c_addr, uint8_t mem_addr)
{
	uint8_t data;

	HAL_I2C_Mem_Read(&hi2c1, i2c_addr, mem_addr, 1, &data, 1, SFP_I2C_TIMEOUT);
	printf("I%d.%d: %d\r\n", i2c_addr, mem_addr, data);
}

void i2c_set(uint8_t i2c_addr, uint8_t mem_addr, uint8_t val)
{

	printf("I%d.%d: %d\r\n", i2c_addr, mem_addr, val);
	HAL_I2C_Mem_Write(&hi2c1, i2c_addr, mem_addr, 1, &val, 1, SFP_I2C_TIMEOUT);
}

void sfp_get(uint8_t addr)
{
	static uint8_t data[SFP_MEM_SIZE];
	uint16_t i;

	printf("SFP:%d ", addr);
	switch (addr) {
	case 0:
		HAL_I2C_Mem_Read(&hi2c1, SFP_I2C_ADDR_1, 0, 1, data, SFP_MEM_SIZE, SFP_I2C_TIMEOUT);
		break;
	case 1:
		HAL_I2C_Mem_Read(&hi2c1, SFP_I2C_ADDR_2, 0, 1, data, SFP_MEM_SIZE, SFP_I2C_TIMEOUT);
		break;
	}

	for (i = 0; i < SFP_MEM_SIZE; i++)
		printf("%02x", data[i]);
	printf("\r\n");
}
