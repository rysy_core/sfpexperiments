#include "parse.h"


uint8_t parse_number(char c, uint8_t *n)
{
    if (c >= '0' && c <= '9') {
        *n = *n * 10 + c - '0';
        return 0;
    }
    if (c == ' ' || c == '\n')
        return 1;
    return -1;
}


void parse_init(state *s)
{
	int8_t i;

	s->state = 0;
	s->code = 0;
	s->num = 0;
	for (i = 0; i < PARSE_NUMS; i++)
		s->n[i] = 0;
}


void parse(state *s, char c)
{
	int res;

	if (s->code == 5 && s->state == 0) {
		s->c = c;
		s->state = 1;
		return;
	}

	if (c == '\r') {
		return;
	}

	if (s->code == 0) {
		switch (c) {
		case 'P':
			s->code = 1;
			break;
		case 'I':
			s->code = 2;
			break;
		case 'G':
			s->code = 3;
			break;
		case 'R':
			s->code = 4;
			break;
		case 'U':
			s->code = 5;
			break;
		case 'B':
			s->code = 6;
			break;
		}
		return;
	}

	if (s->state == 0)
		if (c == ' ')
			return;

	if (s->code != 5 && s->num < PARSE_NUMS) {
		s->state = 1;
		res = parse_number(c, &(s->n[s->num]));
		if (res == 1) {
			s->num += 1;
			s->state = 0;
		} else if (res == -1) {
			parse_init(s);
			return;
		}
	}

	if (c == '\n') {
		switch (s->code) {
		case 1:
			switch (s->num) {
			case 1:
				s->fun_pin_get(s->n[0]);
				break;
			case 2:
				s->fun_pin_set(s->n[0], s->n[1]);
				break;
			}
			break;
		case 2:
			switch (s->num) {
			case 2:
				s->fun_i2c_get(s->n[0], s->n[1]);
				break;
			case 3:
				s->fun_i2c_set(s->n[0], s->n[1], s->n[2]);
				break;
			}
			break;
		case 3:
			s->fun_sfp_get(s->n[0]);
			break;
		case 4:
			s->fun_rec(s->n[0], s->n[1]);
			break;
		case 5:
			s->fun_uart_send(s->c);
			break;
		case 6:
			s->fun_blink();
			break;
		}
		parse_init(s);
	}
}
