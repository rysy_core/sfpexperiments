/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "board.h"
#include "parse.h"
#include "sfp.h"
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
volatile uint8_t usb_data[APP_RX_DATA_SIZE];
volatile uint8_t len = 0;
extern uint8_t rec_data[];
extern uint8_t rec_ready;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
volatile uint8_t b;
static void blink(void)
{
	b = 1;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	state s;
	GPIO_PinState mod_abs, rx_los, tx_fault;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_USB_DEVICE_Init();
  MX_TIM2_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  parse_init(&s);
  s.fun_i2c_get = i2c_get;
  s.fun_i2c_set = i2c_set;
  s.fun_pin_get = pin_get;
  s.fun_pin_set = pin_set;
  s.fun_sfp_get = sfp_get;
  s.fun_uart_send = uart_send;
  s.fun_rec = rec;
  s.fun_blink = blink;

  sfp_init();
  __HAL_TIM_ENABLE_DMA(&htim1, TIM_DMA_CC3);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint8_t i = 0;
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  mod_abs = HAL_GPIO_ReadPin(SFP_PORT, SFP_MOD_ABS);
	  rx_los = HAL_GPIO_ReadPin(SFP_PORT, SFP_RX_LOS);
	  tx_fault = HAL_GPIO_ReadPin(SFP_PORT, SFP_TX_FAULT);

	  HAL_GPIO_WritePin(LED1_PORT, LED1, mod_abs);
	  HAL_GPIO_WritePin(LED_PORT, LED2, rx_los);
	  HAL_GPIO_WritePin(LED_PORT, LED3, tx_fault);

	  if (b == 1) {
		  b = 0;

		  HAL_GPIO_WritePin(LED1_PORT, LED1, 0);
		  HAL_GPIO_WritePin(LED1_PORT, LED2, 0);
		  HAL_GPIO_WritePin(LED1_PORT, LED3, 0);
		  for (i=0; i<10; i++) {
			  HAL_GPIO_WritePin(LED_PORT, LED3, 0);
			  HAL_GPIO_WritePin(LED1_PORT, LED1, 1);
			  HAL_Delay(100);

			  HAL_GPIO_WritePin(LED1_PORT, LED1, 0);
			  HAL_GPIO_WritePin(LED_PORT, LED2, 1);
			  HAL_Delay(100);

			  HAL_GPIO_WritePin(LED_PORT, LED2, 0);
			  HAL_GPIO_WritePin(LED_PORT, LED3, 1);
			  HAL_Delay(100);
		  }
	  }

	  if (rec_ready) {
		  rec_ready = 0;
		  printf("R ");
		  for(i=0; i < REC_BUF_SIZE; i++)
			  printf( (rec_data[i]&(1<<3)) ? "1" : "0");
		  printf("\r\n");
	  }

	  if (len != 0) {
		  for (i = 0; i < len; i++) {
			  parse(&s, usb_data[i]);
		  }
		  len = 0;
	  }

//	  if (__HAL_UART_GET_FLAG(&huart2, UART_FLAG_RXNE) == SET) {
//		  uint8_t v;
//		  HAL_UART_Receive(&huart2, &v, 1, 100);
//		  printf("U%c\r\n", v);
//	  }

	  HAL_Delay(10);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
int _write(int file, char*ptr, int len)
{
	CDC_Transmit_FS((uint8_t *)ptr, len);
	return len;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
