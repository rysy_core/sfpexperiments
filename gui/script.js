'use strict';

let port;
let reader;
let inputDone;
let outputDone;
let inputStream;
let outputStream;


const butConnect = document.getElementById('butConnect');
const divInfo = document.getElementById('info');

const I2CRead = document.getElementById('I2CRead');
const I2CWrite = document.getElementById('I2CWrite');
const pinRead = document.getElementById('pinRead');
const memRead = document.getElementById('memRead');
const blink = document.getElementById('blink');

const I2CDevAddr = document.getElementById('I2CDevAddr');
const I2CAddr = document.getElementById('I2CAddr');
const I2CVal = document.getElementById('I2CVal');

const ModAbs = document.getElementById('ModAbs');
const TxFault = document.getElementById('TxFault');
const RxLOS = document.getElementById('RxLOS');
const TxDisable = document.getElementById('TxDisable');
const RS0 = document.getElementById('RS0');
const RS1 = document.getElementById('RS1'); 
const pins = [TxDisable, RS0, RS1, RxLOS, ModAbs, TxFault];

const a0 = document.getElementById('A0');
const a2 = document.getElementById('A2');
const memInfoH = document.getElementById('memInfoHeader');
const memInfoB = document.getElementById('memInfoBody');

const sendTx = document.getElementById('sendTx');
const getRx = document.getElementById('getRx');
const rxData = document.getElementById('rxData');

var intervalSerial;

document.addEventListener('DOMContentLoaded', () => {
  port = null;
  butConnect.addEventListener('click', clickConnect);
  I2CRead.addEventListener('click', i2c_read);
  I2CWrite.addEventListener('click', i2c_write);
  pinRead.addEventListener('click', pin_read);
  memRead.addEventListener('click', mem_read);
  blink.addEventListener('click', blink_send);
  sendTx.addEventListener('click', send_tx);
  getRx.addEventListener('click', get_rx);

  init_table(a0, sfp_a0, "sfp_a0_");
  init_table(a2, sfp_a2, "sfp_a2_");

  var h = document.getElementById('tab_a0').clientHeight;
  h = "height:" + h + "px"; 
  document.getElementById('tab_TXRX').setAttribute("style", h);

  for (var i = 0; i < pins.length; i++) {
    pins[i].addEventListener('click',
      (function (p) {pin_write(p);}).bind(this, i));
  }

  if (!('serial' in navigator)) {
    setInfo(
      "Sorry, <b>Web Serial</b> is not supported on this device, make sure you're \
       running Chrome 78 or later and have enabled the \
       <code>#enable-experimental-web-platform-features</code> flag in \
       <code>chrome://flags</code>", "alert-danger");
   }

   WaveDrom.editorInit();
   console.log(document.getElementById('rxPlot').innerHTML);
});

async function connect() {
  port = await navigator.serial.requestPort();
  await port.open({ baudRate: 115200 });

  let decoder = new TextDecoderStream();
  inputDone = port.readable.pipeTo(decoder.writable);
  inputStream = decoder.readable
    .pipeThrough(new TransformStream(new LineBreakTransformer()));

  reader = inputStream.getReader();

  const encoder = new TextEncoderStream();
  outputDone = encoder.readable.pipeTo(port.writable);
  outputStream = encoder.writable;

  intervalSerial = window.setInterval(read_serial, 1000);
}

async function disconnect() {
  if (reader) {
    await reader.cancel();
    await inputDone.catch(() => {});
    reader = null;
    inputDone = null;
  }
  if (outputStream) {
    await outputStream.getWriter().close();
    await outputDone;
    outputStream = null;
    outputDone = null;
  }
  await port.close();
  port = null;
  clearInterval(intervalSerial);
}

async function clickConnect() {
  if (port) {
    await disconnect();
    toggleUIConnected(false);
    return;
  }
  await connect();
  toggleUIConnected(true);
}

function writeToStream(...lines) {
  const writer = outputStream.getWriter();
  lines.forEach((line) => {
    console.log('[SEND]', line);
    writer.write(line + '\n');
  });
  writer.releaseLock();
}

function toggleUIConnected(connected) {
  let lbl = 'Connect';
  if (connected) {
    lbl = 'Disconnect';
    setInfo("Connected", "alert-success");
  } else {
    setInfo("Not connected", "alert-dark");
  }
  butConnect.textContent = lbl;
}

class LineBreakTransformer {
  constructor() {
    // A container for holding stream data until a new line.
    this.container = '';
  }

  transform(chunk, controller) {
    // CODELAB: Handle incoming chunk
    this.container += chunk;
    const lines = this.container.split('\n');
    this.container = lines.pop();
    lines.forEach(line => controller.enqueue(line));
  }

  flush(controller) {
    // CODELAB: Flush the stream.
    controller.enqueue(this.container);
  }
}

async function serial_send(cmd) {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  setInfo("Sending: " + cmd, "alert-info");
  writeToStream(cmd);
}

async function send_wait(cmd) {
  setInfo("Sending: " + cmd, "alert-info");
  writeToStream(cmd);
    const { value, done } = await reader.read();
    if (value) {
      console.log(value);
      console.log(value.includes("\n"));
      }
    if (done) {
      console.log('[readLoop] DONE', done);
      reader.releaseLock();
    }

    return value;
}

async function read_serial() {
    const { value, done } = await reader.read();
    if (value) {
      console.log(value);
      if (value[0] == 'P')
        set_pins(value);
      else if(value[0] == 'I')
        set_I2C(value);
      else if(value[0] == 'S')
        set_mem(value);
      else if(value[0] == 'R')
        set_rx(value);
      }
    if (done) {
      console.log('[readLoop] DONE', done);
      reader.releaseLock();
    }
}

function setInfo(text, color) {
  divInfo.innerHTML = text;
  divInfo.className = "alert " + color;
}

async function i2c_read() {
  var t = "I " + I2CDevAddr.value + " " + I2CAddr.value;
  serial_send(t);
}

async function i2c_write() {
  var t = "I " + I2CDevAddr.value + " " + I2CAddr.value + " " + I2CVal.value;
  serial_send(t);
}

async function set_I2C(ret) {
  var data = ret.split(":");
  I2CVal.value = parseInt(data[1]);
}

async function pin_read() {
  serial_send("P 10");
}

async function pin_write(p) {
  var cmd = "P " + p + " " + (pins[p].checked ? "1" : "0");
  serial_send(cmd);
}

async function set_pins(ret) {
  if (ret[0] != 'P' || ret[1] != '1' || ret[2] != '0')
    return;

  var retInt = parseInt(ret.split(":")[1]);

  for (var i = 0; i < pins.length; i++){
    pins[i].checked = retInt & (1 << i);
  }
}

async function mem_read() {
  serial_send("G 0");
  setTimeout(() => { serial_send("G 1"); }, 2000);
}

async function blink_send() {
  serial_send("B");
}

async function send_tx() {
  var cmd = "R 0 " + (sendTx.checked ? "1" : "0");
  serial_send(cmd);
}

async function get_rx() {
  serial_send("R 1");
}

async function set_mem(read_data) {
  var data = read_data.split(" ")[1];
  if (read_data[4] == '0')
    update_table(sfp_a0, "sfp_a0_", data);
  else if(read_data[4] == '1')
    update_table(sfp_a2, "sfp_a2_", data);
}

async function set_rx(data) {
  if (data[1] != " ")
    return;
  
  var s = data.split(" ")[1];
  var ls = s.length/2;
  
  rxData.innerHTML = s.slice(0, ls) + "<br>" + s.slice(ls);

  var source = {signal: []};

  for (var i=0; i<s.length-1; i+=20) {
    var pi = s[i];
    for (var j=1; j < 20; j++) {
      if (i+j >= s.length)
        break;
      if (s[i+j-1] == s[i+j])
        pi += ".";
      else
        pi += s[i+j];
    }
    source.signal.push({name: "P" + i/20, wave: pi});
  }

  console.log(source);
  WaveDrom.RenderWaveForm(0, source, 'WaveDrom_Display_'); 

}

function update_table(sfp, id, data) {
  var col_v = 4;
  var col_m = 5;

  for (var i = 0; i < sfp.length; i++) {
    var addr = sfp[i]['addr'];
    var len = sfp[i]['bytes'];
    
    var val = "0x" + data.slice(2*addr, 2*(addr+len));
    var row = document.getElementById(id + i);
    var info = sfp[i]['info'];

    if (sfp[i]['type'] == 'list') {
      row.cells[col_v].innerHTML = val;
      var val_int  = parseInt(val);
      for (var j = 0; j < info.length; j++)
        if (val_int >= info[j].min && val_int <= info[j].max) {
          row.cells[col_m].innerHTML = info[j]['desc'];
          break;
        }
    } else if (sfp[i]['type'] == 'bits') {
      var txt = "<table>";
      txt += "<tr><th>bit</th><th>val</th><th>description</th></tr>";
      for (var j = 0; j < info.length; j++) {
        txt += "<tr>";
        var byte_id = info[j]['byte_id'];
        if (byte_id == -1) {
          txt += "<th colSpan=3 >" + info[j]['desc'] + "</th>";
        } else {
          var bit_id = info[j]['bit_id'];
          var b = parseInt("0x" + data.slice(2*(addr+byte_id), 2*(addr+byte_id+1)));
          var state = (b & (1<<bit_id)) != 0;
          var chBox = "";
          if (state)
            chBox = "<input class='form-check-input' type='checkbox' checked disabled>";
          else
            chBox = "<input class='form-check-input' type='checkbox' disabled>";
          txt += "<td>" + byte_id + "." +bit_id + "</td><td>" + chBox + "</td><td>" + info[j]['desc'] + "</td>";
        }
        txt += "</tr>";
      }
      if (row.length > col_m)
          row.deleteCell(col_m);
      row.cells[col_v].colSpan = 2;
      row.cells[col_v].innerHTML = txt;
    } else if (sfp[i]['type'] == 'unit') {
      var valInt  = parseInt(val);
      if (info['sign'])
        if (valInt >= (1 << (8*len-1))-1)
          valInt -= (1 << (8*len));
      valInt *= info['multiplier'];
      row.cells[col_v].innerHTML = val;
      row.cells[col_m].innerHTML = "" + valInt + " " + info['unit'];
    } else if (sfp[i]['type'] == 'ASCII') {
      var txt = to_ascii(val, len);
      if (row.length > col_m)
          row.deleteCell(col_m);
      row.cells[col_v].colSpan = 2;
      row.cells[col_v].innerHTML = val + "</br>" + txt;
    } else if (sfp[i]['type'] == 'OUI') {
      row.cells[col_v].innerHTML = val;
      row.cells[col_m].innerHTML = val.slice(2,4) + "." + val.slice(4,6) + "." + val.slice(6,8);
    } else if (sfp[i]['type'] == 'raw') {
      if (row.length > col_m)
        row.deleteCell(col_m);
      row.cells[col_v].colSpan = 2;

      val = val.slice(2);
      var txt = ""
      var in_row = 32;
      var j;
      for (j = 0; j < val.length; j+=in_row)
        if (j+in_row < val.length)
          txt += val.slice(j, j+in_row) + "</br>";
        else
          txt += val.slice(j);
          
      row.cells[col_v].innerHTML = txt;
    } else if (sfp[i]['type'] == 'date') {
      var txt = to_ascii(val, len);
      var year = txt.slice(0, 2);
      var month = txt.slice(2, 4);
      var day = txt.slice(4, 6);
      var lot_code = txt.slice(6, 8);
      row.cells[col_v].innerHTML = val;
      row.cells[col_m].innerHTML = year + "." + month + "." + day + " lot code: " + lot_code;
    }
  }
}

function to_ascii(val, len) {
  var txt = ""

  for (var j = 0; j < len; j++) {
    var n = parseInt("0x" + val.slice(2*(j+1), 2*(j+2)));
    txt += String.fromCharCode(n);
  }

  return txt;
}

function show_mem_info(elmnt) {
  memInfoH.innerHTML = elmnt.getAttribute('title');
  memInfoB.innerHTML = elmnt.getAttribute('data-bs-content');
}

function init_table(tab, sfp, id) {
  tab.style += "width:100%;";

  var header = tab.createTHead();
  header.style = "display: block; width: 100%;";

  var rowH = header.insertRow(0);
  rowH.width = "100%";

  var width = ["5%", "6%", "6%", "20%", "28%", "35%"];
  for (var i = 0; i < sfp_col.length; i++) {
    var cell = rowH.insertCell(i);
    cell.scope = 'col';
    cell.innerHTML = sfp_col[i];
    cell.width = width[i];
  }

  var body = tab.createTBody();
  body.style = "display: block;overflow-y: auto;overflow-x: hidden; height: 500px; width: 100%;";

  for (var i = 0; i < sfp.length; i++) {
    var row = body.insertRow(i);
    row.width = "100%";

    row.setAttribute('id', id + i);
    row.setAttribute('title', sfp[i]["name"]);

    var info = "<p>" + sfp[i]["desc"] + "</p>";

    if (sfp[i]['type'] == 'list') {
      info += "<table><tr><th>min</th><th>max</th><th>meaning</th></tr>";
      for (var j = 0; j < sfp[i]['info'].length; j++) {
        var v = sfp[i]['info'][j];
        info += "<tr><td>0x" + v['min'].toString(16) + "</td><td>0x" + v['max'].toString(16) + "</td><td>" + v['desc'] + "</td></tr>";
      }
      info += "</table>";
    } else if (sfp[i]['type'] == 'bits') {
      info += "<table><tr><th>byte</th><th>bit</th><th>meaning</th></tr>";
      for (var j = 0; j < sfp[i]['info'].length; j++) {
        var v = sfp[i]['info'][j];
        if (v['byte_id'] == -1)
          info += "<tr><th colSpan=2 >" + v['desc'] + "</th></tr>";
        else
          info += "<tr><td>" + v['byte_id'] + "</td><td>" + v['bit_id'] + "</td><td>" + v['desc'] + "</td></tr>";
      }
      info += "</table>";
    } else if (sfp[i]['type'] == 'unit') {
      info += "<table>";
      info += "<tr><td>unit</td><td>" + sfp[i]['info']['unit'] + "</td></tr>";
      info += "<tr><td>sign</td><td>" + ((sfp[i]['info']['sign']) ? "signed" : "unsigned" ) + "</td></tr>";
      info += "<tr><td>multiplier</td><td>" + sfp[i]['info']['multiplier'] + "</td></tr>";
      info += "</table>";
    } else if (sfp[i]['type'] == 'ASCII') {
    } else if (sfp[i]['type'] == 'OUI') {
    } else if (sfp[i]['type'] == 'date') {
      for (var j = 0; j < sfp[i]['info'].length; j++)
        info += "<p>" + sfp[i]['info'][j] + "</p>";
    }
    row.setAttribute('data-bs-content', info);
    row.setAttribute('onclick', 'show_mem_info(this)');

    for (var j = 0; j < sfp_col.length; j++) {
      var cell = row.insertCell(j);
      cell.width = width[j];

      if (sfp_col[j] in sfp[i])
        cell.innerHTML = sfp[i][sfp_col[j]];

      switch (j) {
        case 0:
          cell.scope = 'row';
          cell.innerHTML = i;
          break;
      } 
    }
  }
 
  for (var i = 0; i < rowH.cells.length; i++)
    rowH.cells[i].width = body.rows[0].cells[i].width;
  rowH.cells[rowH.cells.length-1].width = "";
}

